package jp.co.alhinc.form;

import java.util.Date;

public class ReserveForm {

	private int id;
	private int book_id;
	private int user_id;
	private Date reserved_date;
	private int lending;
	private Date arrive_date;
	private int status;

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Date getReserve_date() {
		return reserved_date;
	}
	public void setReserve_date(Date reserve_date) {
		this.reserved_date = reserve_date;
	}
	public int getLending() {
		return lending;
	}
	public void setLending(int lending) {
		this.lending = lending;
	}
	public Date getArrive_date() {
		return arrive_date;
	}
	public void setArrive_date(Date arrive_date) {
		this.arrive_date = arrive_date;
	}



}
