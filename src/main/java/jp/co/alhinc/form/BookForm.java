package jp.co.alhinc.form;

import java.util.Date;

public class BookForm {

	private int id;

//	@NotNull
	private int lib_num;
//	@NotNull(message="棚番号を入力してください")
	private int shelf_num;
//	@Size(max=17)
//	@NotNull(message="ISBNを入力してください")
	private String isbn;
//	@NotBlank (message="書名を入力してください")
//	@Size(max=100, message="書名は{max}文字以内で入力してください")
	private String book_name;
//	@NotBlank(message="著者名を入力してください")
//	@Size(max=30, message="著者名は{max}文字以内で入力してください")
	private String author;
//	@NotBlank(message="出版社を入力してください")
//	@Size(max=20, message="出版社は{max}文字以内で入力してください")
	private String publisher;
	private int category;
	private int genre;
	private int status;
	private Date updated_date;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getLib_num() {
		return lib_num;
	}
	public void setLib_num(int lib_num) {
		this.lib_num = lib_num;
	}
	public int getShelf_num() {
		return shelf_num;
	}
	public void setShelf_num(int shelf_num) {
		this.shelf_num = shelf_num;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public int getGenre() {
		return genre;
	}
	public void setGenre(int genre) {
		this.genre = genre;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Date getUpdated_date() {
		return updated_date;
	}
	public void setUpdated_date(Date updated_date) {
		this.updated_date = updated_date;
	}

}
