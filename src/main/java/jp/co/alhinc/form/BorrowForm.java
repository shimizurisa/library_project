package jp.co.alhinc.form;

import java.util.Date;



public class BorrowForm {



	private int id;
	private int book_id;
	private String book_name;
	private String author;
	private int status;
	private String status_name;
	private int user_id;
	private String user_name;
	private String user_num;
	private Date borrow_date;
	private Date return_schedule;
	private String name;
	private Date reserved_date;
	private Date arrive_date;
	private Date return_date;
	private Date update_date;





	public Date getReturn_date() {
		return return_date;
	}
	public void setReturn_date(Date return_date) {
		this.return_date = return_date;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date updated_date) {
		this.update_date = updated_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getStatus_name() {
		return status_name;
	}
	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	public String getUser_num() {
		return user_num;
	}
	public void setUser_num(String user_num) {
		this.user_num = user_num;
	}
	public Date getBorrow_date() {
		return borrow_date;
	}
	public void setBorrow_date(Date borrow_date) {
		this.borrow_date = borrow_date;
	}
	public Date getReturn_schedule() {
		return return_schedule;
	}
	public void setReturn_schedule(Date return_schedule) {
		this.return_schedule = return_schedule;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getReserved_date() {
		return reserved_date;
	}
	public void setReserved_date(Date reserved_date) {
		this.reserved_date = reserved_date;
	}
	public Date getArrive_date() {
		return arrive_date;
	}
	public void setArrive_date(Date arrive_date) {
		this.arrive_date = arrive_date;
	}


}
