package jp.co.alhinc.mapper;

import java.util.List;

import jp.co.alhinc.entity.BookEntity;
import jp.co.alhinc.entity.CategoryEntity;
import jp.co.alhinc.entity.GenreEntity;
import jp.co.alhinc.entity.LibraryEntity;
import jp.co.alhinc.entity.StatusEntity;
import jp.co.alhinc.form.BookForm;

public interface BookMapper {

//	int insertTest(String name);

	BookEntity getBook(int id);

	List<BookEntity> getBookAll(BookForm searchForm);

	void bookRegister(BookEntity entity);

	void updateBook(BookEntity entity);

	void changeStatus(BookForm bookForm);

	List<LibraryEntity> getLibrary();

	List<CategoryEntity> getCategory();

	List<GenreEntity> getGenre();

	List<StatusEntity> getStatus();



}
