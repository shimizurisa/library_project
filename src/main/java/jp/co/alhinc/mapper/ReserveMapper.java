package jp.co.alhinc.mapper;

import jp.co.alhinc.entity.ReserveEntity;
import jp.co.alhinc.form.ReserveForm;

public interface ReserveMapper {

	void reserve(ReserveEntity entity);

	int countReserved(ReserveForm form);

	void changeLending(ReserveForm reserveForm);




}
