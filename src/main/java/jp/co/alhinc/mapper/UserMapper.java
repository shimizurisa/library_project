

package jp.co.alhinc.mapper;


import java.util.List;

import jp.co.alhinc.entity.GenderEntity;
import jp.co.alhinc.entity.LibraryEntity;
import jp.co.alhinc.entity.UserEntity;
import jp.co.alhinc.form.UserForm;

public interface UserMapper {


	UserEntity getUser(int id);

	List<UserEntity> getUserAll(UserForm form);



	void userRegister(UserEntity entity);
	void updateUserEdit(UserEntity entity);

	List<GenderEntity> getGender();

	List<LibraryEntity> getLibrary();

	UserEntity getLogin(String num);

	List<UserEntity> getLoginAll(String num);

	UserEntity getInfo(int id);

	int count(UserForm form);


}