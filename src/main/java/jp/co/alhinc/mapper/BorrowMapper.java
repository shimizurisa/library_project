package jp.co.alhinc.mapper;

import java.util.List;

import jp.co.alhinc.entity.BorrowEntity;
import jp.co.alhinc.form.BorrowForm;

public interface BorrowMapper {

	BorrowEntity getBorrow(int id);

	void borrowRegister(BorrowEntity entity);
	List<BorrowEntity>getReturn(BorrowForm form);
	List<BorrowEntity>getReturnNew(BorrowForm form);
	List<BorrowEntity> getReserved(BorrowForm form);

	void updateStatus(BorrowForm borrowForm);


}