package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.entity.BookEntity;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.mapper.BookMapper;

@Service
public class AdminBookListService {

	@Autowired
	public BookMapper bookMapper;

	public List<BookDto> getBookAll(BookForm searchForm){

		List<BookEntity> bookList = bookMapper.getBookAll(searchForm);
		List<BookDto> resultList = convertToDto(bookList);

		return resultList;
	}

	private List<BookDto> convertToDto(List<BookEntity> bookList){

		List<BookDto> resultList = new LinkedList<>();

		for (BookEntity entity : bookList){

			BookDto dto = new BookDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}

		return resultList;
	}


	public void changeStatus(BookForm bookForm){

		bookMapper.changeStatus(bookForm);
	}
}
