package jp.co.alhinc.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.entity.UserEntity;
import jp.co.alhinc.mapper.UserMapper;

@Service
public class LoginService {

	@Autowired
	private UserMapper userMapper;

	public UserDto getLogin(String num){
		UserDto dto =new UserDto();
		List<UserEntity> entity = userMapper.getLoginAll(num);
		System.out.println(entity.size());
		if(entity.size() == 0){
			return null;

		}

//		System.out.println(entity.size());

		BeanUtils.copyProperties(entity.get(0), dto);
	    return dto;

	}
//	public List<UserDto> getLoginAll() {
/*        List<UserEntity> userList = userMapper.getLoginAll();*/
/*        List<UserDto> resultList = convertToDto(userList);
        return resultList;*/
//		return null;
//	}


}
