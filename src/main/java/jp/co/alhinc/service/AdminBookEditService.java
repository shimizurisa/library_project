package jp.co.alhinc.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.entity.BookEntity;
import jp.co.alhinc.mapper.BookMapper;

@Service

public class AdminBookEditService {

	@Autowired
	private BookMapper bookMapper;

	public BookDto getBook(int id){


		BookEntity entity = bookMapper.getBook(id);
		BookDto dto = new BookDto();

		BeanUtils.copyProperties(entity, dto);

		return dto;


	}

	public void updateBook(BookDto dto){

		BookEntity entity = new BookEntity();

		BeanUtils.copyProperties(dto, entity);

		bookMapper.updateBook(entity);

	}

	/*public List<BookDto> convertToDto(List<BookEntity> bookList){

		List<BookDto> resultList = new LinkedList<>();

		for (BookEntity entity : bookList){

			BookDto dto = new BookDto();

			BeanUtils.copyProperties(entity, dto);

			resultList.add(dto);
		}

		return resultList;
	}*/

}
