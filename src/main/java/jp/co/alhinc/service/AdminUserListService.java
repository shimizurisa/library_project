package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.entity.UserEntity;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.mapper.UserMapper;


@Service
public class AdminUserListService {

	@Autowired
	private UserMapper userMapper;

	public List<UserDto>getUserAll(UserForm form){

		List<UserEntity> userList = userMapper.getUserAll(form);
		List<UserDto>resultList = convertToDto(userList);
		return resultList;
	}

	private List<UserDto> convertToDto(List<UserEntity> userList){
		List<UserDto> resultList = new LinkedList<>();
		for(UserEntity entity : userList){

			UserDto dto = new UserDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}
}
