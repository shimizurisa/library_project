package jp.co.alhinc.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.entity.BookEntity;
import jp.co.alhinc.mapper.BookMapper;

@Service
public class BookSignUpService {

	@Autowired
	private BookMapper bookMapper;


	public void bookRegister(BookDto dto){

		BookEntity entity = new BookEntity();

		BeanUtils.copyProperties(dto, entity);

		bookMapper.bookRegister(entity);

	}
}
