package jp.co.alhinc.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.ReserveDto;
import jp.co.alhinc.entity.ReserveEntity;
import jp.co.alhinc.form.ReserveForm;
import jp.co.alhinc.mapper.ReserveMapper;

@Service

public class ReserveService {

	@Autowired
	private ReserveMapper reserveMapper;


	public void reserve(ReserveDto dto){

		ReserveEntity entity = new ReserveEntity();
		BeanUtils.copyProperties(dto, entity);

		reserveMapper.reserve(entity);
	}

	public int countReserved(ReserveForm form){
//		List<ReserveEntity> reservedList = reserveMapper.getReserved(form);
//		List<ReserveDto>resultList = convertToDto(reservedList);

		int reserveUser = reserveMapper.countReserved(form);

		return reserveUser;
	}

	public void changeLending(ReserveForm reserveForm){

		reserveMapper.changeLending(reserveForm);
	}



//	private List<ReserveDto> convertToDto(List<ReserveEntity> reservedList){
//		List<ReserveDto> resultList = new LinkedList<>();
//		for(ReserveEntity entity : reservedList){
//
//		ReserveDto dto = new ReserveDto();
//		BeanUtils.copyProperties(entity, dto);
//		resultList.add(dto);
//	}
//		return resultList;
//	}

}
