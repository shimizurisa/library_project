package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.CategoryDto;
import jp.co.alhinc.entity.CategoryEntity;
import jp.co.alhinc.mapper.BookMapper;

@Service

public class CategoryService {

	@Autowired
	private BookMapper bookMapper;

	public List<CategoryDto> getCategory(){

		List<CategoryEntity> categoryList = bookMapper.getCategory();
		List<CategoryDto> resultList = convertToDto(categoryList);

		return resultList;

	}

	public List<CategoryDto> convertToDto (List<CategoryEntity> categoryList){

		List<CategoryDto> resultList = new LinkedList<>();

		for (CategoryEntity entity : categoryList){

			CategoryDto dto = new CategoryDto();

			BeanUtils.copyProperties(entity, dto);

			resultList.add(dto);

		}

		return resultList;
	}

}
