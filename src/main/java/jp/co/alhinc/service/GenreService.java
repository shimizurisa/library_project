package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.GenreDto;
import jp.co.alhinc.entity.GenreEntity;
import jp.co.alhinc.mapper.BookMapper;

@Service

public class GenreService {

	@Autowired
	public BookMapper bookMapper;

	public List<GenreDto> getGenre(){

		List<GenreEntity> genreList = bookMapper.getGenre();

		List<GenreDto> resultList = convertToDto(genreList);
		System.out.println(genreList);

		return resultList;

	}

	private List<GenreDto> convertToDto(List<GenreEntity> genreList){

		List<GenreDto> resultList = new LinkedList<>();

		for (GenreEntity entity : genreList){

			GenreDto dto = new GenreDto();
			BeanUtils.copyProperties(entity, dto);

			resultList.add(dto);
		}

		return resultList;

	}


}
