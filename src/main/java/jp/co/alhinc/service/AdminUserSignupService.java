package jp.co.alhinc.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.entity.UserEntity;
import jp.co.alhinc.mapper.UserMapper;

@Service
public class AdminUserSignupService {

	@Autowired
	private UserMapper userMapper;


	public void userRegister(UserDto dto){

		UserEntity entity = new UserEntity();

		BeanUtils.copyProperties(dto, entity);

		userMapper.userRegister(entity);
	}

}
