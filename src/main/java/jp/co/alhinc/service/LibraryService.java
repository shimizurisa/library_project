package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.entity.LibraryEntity;
import jp.co.alhinc.mapper.BookMapper;

@Service

public class LibraryService {

	@Autowired
	private BookMapper bookMapper;

	public List<LibraryDto> getLibrary(){

		List<LibraryEntity> libraryList = bookMapper.getLibrary();
		List<LibraryDto> resultList = convertToDto(libraryList);

		return resultList;

	}

	public List<LibraryDto> convertToDto (List<LibraryEntity> libraryList){

		List<LibraryDto> resultList = new LinkedList<>();

		for(LibraryEntity etity : libraryList){

			LibraryDto dto = new LibraryDto();
			BeanUtils.copyProperties(etity, dto);

			resultList.add(dto);

		}

		return resultList;
	}

}
