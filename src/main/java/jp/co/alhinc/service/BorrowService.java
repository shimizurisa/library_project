package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.BorrowDto;
import jp.co.alhinc.entity.BorrowEntity;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.form.BorrowForm;
import jp.co.alhinc.mapper.BookMapper;
import jp.co.alhinc.mapper.BorrowMapper;

@Service
public class BorrowService {

	@Autowired
	private BorrowMapper borrowMapper;

	@Autowired
	private BookMapper bookMapper;	public void borrowRegister(BorrowDto dto){

		BorrowEntity entity = new BorrowEntity();

		BeanUtils.copyProperties(dto, entity);

		borrowMapper.borrowRegister(entity);
	}



	public void changeStatus(BookForm bookForm){

		bookMapper.changeStatus(bookForm);
	}






	public List<BorrowDto>getReturn(BorrowForm form){

		List<BorrowEntity> borrowList = borrowMapper.getReturn(form);
		List<BorrowDto> borrowResult = convertToDto(borrowList);

		return borrowResult;
	}

	private List<BorrowDto> convertToDto(List<BorrowEntity> borrowList){

		List<BorrowDto> borrowResult = new LinkedList<>();

		for (BorrowEntity entity : borrowList){

			BorrowDto borrowdto = new BorrowDto();
			BeanUtils.copyProperties(entity, borrowdto);
			borrowResult.add(borrowdto);
		}

		return borrowResult;
	}




	public List<BorrowDto>getReturnNew(BorrowForm form){

		List<BorrowEntity> borrowListNew = borrowMapper.getReturnNew(form);
		List<BorrowDto> borrowResultNew = convertToDtoNew(borrowListNew);

		return borrowResultNew;
	}

	private List<BorrowDto> convertToDtoNew(List<BorrowEntity> borrowListNew){

		List<BorrowDto> borrowResultNew = new LinkedList<>();

		for (BorrowEntity entity : borrowListNew){

			BorrowDto borrowdtoNew = new BorrowDto();
			BeanUtils.copyProperties(entity, borrowdtoNew);
			borrowResultNew.add(borrowdtoNew);
		}

		return borrowResultNew;
	}
}