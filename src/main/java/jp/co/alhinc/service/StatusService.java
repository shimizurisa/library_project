package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.StatusDto;
import jp.co.alhinc.entity.StatusEntity;
import jp.co.alhinc.mapper.BookMapper;

@Service
public class StatusService {

	@Autowired
	public BookMapper bookMapper;

	public List<StatusDto> getStatus(){

		List<StatusEntity> statusList = bookMapper.getStatus();
		System.out.println(statusList.size());
		List<StatusDto> resultList = convertToDto(statusList);
		System.out.println(statusList);

		return resultList;

	}

	private List<StatusDto> convertToDto(List<StatusEntity> statusList){

		List<StatusDto> resultList = new LinkedList<>();

		for (StatusEntity entity : statusList){

			StatusDto dto = new StatusDto();
			BeanUtils.copyProperties(entity, dto);

			resultList.add(dto);
		}

		return resultList;

	}

}
