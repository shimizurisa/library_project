package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.GenderDto;
import jp.co.alhinc.entity.GenderEntity;
import jp.co.alhinc.mapper.UserMapper;

@Service
public class GenderService {

	@Autowired
	public UserMapper userMapper;

	public List<GenderDto> getGender(){

		List<GenderEntity> genderList = userMapper.getGender();

		List<GenderDto> resultList = convertToDto(genderList);
		System.out.println(genderList);

		return resultList;

	}

	private List<GenderDto> convertToDto(List<GenderEntity> genderList){

		List<GenderDto> resultList = new LinkedList<>();

		for (GenderEntity entity : genderList){

			GenderDto dto = new GenderDto();
			BeanUtils.copyProperties(entity, dto);

			resultList.add(dto);
		}

		return resultList;

	}

}
