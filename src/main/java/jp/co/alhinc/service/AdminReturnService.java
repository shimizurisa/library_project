package jp.co.alhinc.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.alhinc.dto.BorrowDto;
import jp.co.alhinc.entity.BorrowEntity;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.form.BorrowForm;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.mapper.BookMapper;
import jp.co.alhinc.mapper.BorrowMapper;
import jp.co.alhinc.mapper.UserMapper;

@Service
public class AdminReturnService {

	@Autowired
	private BorrowMapper borrowMapper;
	@Autowired
	public BookMapper bookMapper;
	@Autowired
	public UserMapper userMapper;

/*	借りている本*/
	public List<BorrowDto>getBorrowList(BorrowForm form){

		List<BorrowEntity> borrowList = borrowMapper.getReturn(form);

		List<BorrowDto>resultList = convertToDto(borrowList);

		return resultList;
	}

	private List<BorrowDto> convertToDto(List<BorrowEntity> borrowList){

		List<BorrowDto> resultList = new LinkedList<>();

		for(BorrowEntity entity : borrowList){

			BorrowDto dto = new BorrowDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	/*予約している本*/
	public List<BorrowDto>getReservedList(BorrowForm form){

		List<BorrowEntity> reservedList = borrowMapper.getReserved(form);
		List<BorrowDto>resultList = convertToDto2(reservedList);

		return resultList;
	}

	private List<BorrowDto> convertToDto2(List<BorrowEntity> reservedList){

		List<BorrowDto> resultList = new LinkedList<>();

		for(BorrowEntity entity : reservedList){

			BorrowDto dto = new BorrowDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}


	/*返却切り替え*/
/*	public void updateReturn(BookDto dto){

		BookEntity entity = new BookEntity();

		BeanUtils.copyProperties(dto, entity);

		bookMapper.changeStatus(entity);

	}*/

	public void changeStatus(BookForm bookForm){

		bookMapper.changeStatus(bookForm);
	}

	public void updateStatus(BorrowForm borrowForm){

		borrowMapper.updateStatus(borrowForm);
	}

	public int count(UserForm userForm){

		int userCount = userMapper.count(userForm);
		return userCount;

	}
}




