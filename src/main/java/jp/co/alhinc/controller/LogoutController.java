package jp.co.alhinc.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LogoutController {

	@Autowired
	HttpSession session;

	@RequestMapping(value="/logout/", method=RequestMethod.GET)
	public String logout(Model model){

		session.invalidate();

		return "redirect:/";
	}

}
