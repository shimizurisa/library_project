package jp.co.alhinc.controller;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.LoginService;


@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@Autowired
	HttpSession session;

	@RequestMapping(value = "/login/", method = RequestMethod.GET)
	public String login(Model model) {
		UserForm userForm = new UserForm();
		model.addAttribute("userForm", userForm);
		return "login";

	}


	@RequestMapping(value = "/login/", method = RequestMethod.POST)
	public String login(@ModelAttribute("userForm") UserForm form, Model model){
		String userNum = form.getNum();

//		System.out.println(userNum);

		UserDto dto = loginService.getLogin(userNum);

//		System.out.println(dto.getId());
//		System.out.println(dto.getNum());



		if(dto != null){
//			BeanUtils.copyProperties(form, dto);
			session.setAttribute("loginUser", dto);
			return "redirect:/reserve/" + session.getAttribute("book_id").toString();

		} else {
			List<String> messages = new ArrayList<String>();
	        messages.add("ログインに失敗しました");
	        return "login";

		}

	}


}
