package jp.co.alhinc.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.GenderDto;
import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminUserSignupService;
import jp.co.alhinc.service.GenderService;
import jp.co.alhinc.service.LibraryService;



@Controller
public class AdminUserSignupController {

	@Autowired
	private AdminUserSignupService adminUserSignupService;

	@Autowired
	private GenderService genderService;

	@Autowired
	private LibraryService libraryService;

	@RequestMapping (value = "/admin/userSignup/", method = RequestMethod.GET)
	public String adminUserSignup(Model model){

		UserForm form = new UserForm();
		List<GenderDto> gender = genderService.getGender();
		List<LibraryDto> library = libraryService.getLibrary();

		model.addAttribute("genders", gender);
		model.addAttribute("libraries", library);
		model.addAttribute("userForm", form);

		return "adminUserSignup";


	}


	@RequestMapping (value = "/admin/userSignup/", method = RequestMethod.POST)
	public String adminUserSignup(@ModelAttribute UserForm form, Model model){

		UserDto dto = new UserDto();

		BeanUtils.copyProperties(form, dto);
		adminUserSignupService.userRegister(dto);

		return "redirect:/admin/userList/";
	}




}
