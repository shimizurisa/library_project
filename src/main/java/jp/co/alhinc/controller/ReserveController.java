package jp.co.alhinc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.dto.ReserveDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.form.ReserveForm;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminBookListService;
import jp.co.alhinc.service.AdminUserListService;
import jp.co.alhinc.service.ReserveService;

@Controller

public class ReserveController {

	@Autowired
	private AdminBookListService adminBookListService;

	@Autowired
	private AdminUserListService adminUserListService;

	@Autowired
	private ReserveService reserveService;



	@Autowired
	HttpSession session;


	@RequestMapping(value="/reserve/{id}", method = RequestMethod.GET)
	public String reserve(Model model, @PathVariable int id){

		UserDto loginUser = (UserDto) session.getAttribute("loginUser");
		if(loginUser == null){

			session.setAttribute("book_id", id);

			return "redirect:/login/";

		}

		BookForm form = new BookForm();
		form.setId(id);

		List<BookDto> bookList = adminBookListService.getBookAll(form);

		List<String> messages = new ArrayList<String>();

		if(bookList.isEmpty() == true){

			messages.add("不正なパラメーターです。");
			model.addAttribute("reserveBook", new BookDto());


		}else if (2 <= bookList.size()){
			model.addAttribute("reserveBook", new BookDto());

			throw new IllegalStateException("2 <= userList.size()");


		}else {
			model.addAttribute("reserveBook", bookList.get(0));

		}

		UserForm userForm = new UserForm();

		userForm.setId(loginUser.getId());

		List<UserDto> userList = adminUserListService.getUserAll(userForm);
		System.out.println(userList.size());


		if(userList.isEmpty() == true){

			messages.add("不正なパラメーターです");
			model.addAttribute("reserveUser", new UserDto());
			session.setAttribute("messages", messages);


		}else if (2 <= userList.size()){
			model.addAttribute("reserveUser", new UserDto());

			throw new IllegalStateException("2 <= userList.size()");


		}else {
			model.addAttribute("reserveUser", userList.get(0));

		}

		ReserveForm reserveForm = new ReserveForm();
		reserveForm.setBook_id(id);
		reserveForm.setUser_id(loginUser.getId());
		reserveForm.setLending(0);

		int reservedUser = reserveService.countReserved(reserveForm);

		if(reservedUser != 0){

			messages.add("既に予約しています");
			session.setAttribute("messages", messages);
			return "redirect:/search/";

		}


		return "reserve";

	}


	@RequestMapping(value="/reserve/{id}", method=RequestMethod.POST)
	public String reserve(Model model, @ModelAttribute("reserveForm") ReserveForm form){

		ReserveDto dto = new ReserveDto();
		BeanUtils.copyProperties(form, dto);
		reserveService.reserve(dto);

		List<String> messages = new ArrayList<String>();
		messages.add("予約が完了しました");
		session.setAttribute("messages", messages);


		return "redirect:/search/";

	}




}
