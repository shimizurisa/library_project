package jp.co.alhinc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.dto.CategoryDto;
import jp.co.alhinc.dto.GenreDto;
import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.dto.StatusDto;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.service.AdminBookListService;
import jp.co.alhinc.service.CategoryService;
import jp.co.alhinc.service.GenreService;
import jp.co.alhinc.service.LibraryService;
import jp.co.alhinc.service.StatusService;

@Controller
public class AdminBookListController {

	@Autowired
	private AdminBookListService adminBookListService;

	@Autowired
	private StatusService statusService;

	@Autowired
	private LibraryService libraryService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private GenreService genreService;


	@RequestMapping(value = "/admin/bookList/", method = RequestMethod.GET)
	public String bookAll(Model model, @ModelAttribute ("searchForm") BookForm searchForm){

		//プルダウン、ラジオボタンの項目の情報を持ってきた
		List<LibraryDto> library = new ArrayList<>();
		LibraryDto libDto = new LibraryDto();
		libDto.setId(0);
		libDto.setName("すべて");
		library.add(libDto);
		List<LibraryDto> library2 = libraryService.getLibrary();
		library.addAll(library2);

		model.addAttribute("libraries", library);

		//カテゴリーのプルダウン項目作成
		List<CategoryDto> category = new ArrayList<>();
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setId(0);
		categoryDto.setName("すべて");
		category.add(categoryDto);
		List<CategoryDto> category2 = categoryService.getCategory();
		category.addAll(category2);
		model.addAttribute("categories", category);

		//ジャンルのプルダウン項目作成
		List<GenreDto> genre = new ArrayList<>();
		GenreDto genreDto = new GenreDto();
		genreDto.setId(0);
		genreDto.setName("すべて");
		genre.add(genreDto);
		List<GenreDto> genre2 = genreService.getGenre();
		genre.addAll(genre2);

		model.addAttribute("genres", genre);
		//状態のプルダウン項目作成
		List<StatusDto> status = new ArrayList<>();
		StatusDto statusDto = new StatusDto();
		statusDto.setId(0);
		statusDto.setName("すべて");
		status.add(statusDto);
		List<StatusDto> status2 = statusService.getStatus();
		status.addAll(status2);

		model.addAttribute("statuses", status);



		model.addAttribute("searchForm", searchForm);



		//図書一覧の情報をDBから取ってきた
		List <BookDto> books = adminBookListService.getBookAll(searchForm);
		model.addAttribute("books", books);


		return "adminBookList";
	}


	@RequestMapping (value="/admin/bookList/", method = RequestMethod.POST)
	public String bookStatus(Model model, @ModelAttribute("bookForm") BookForm bookForm){


		adminBookListService.changeStatus(bookForm);


		return "redirect:/admin/bookList/";


	}
}
