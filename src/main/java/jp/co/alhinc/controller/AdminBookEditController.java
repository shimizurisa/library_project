package jp.co.alhinc.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.dto.CategoryDto;
import jp.co.alhinc.dto.GenreDto;
import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.dto.StatusDto;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.service.AdminBookEditService;
import jp.co.alhinc.service.CategoryService;
import jp.co.alhinc.service.GenreService;
import jp.co.alhinc.service.LibraryService;
import jp.co.alhinc.service.StatusService;

@Controller

public class AdminBookEditController {

	@Autowired
	private AdminBookEditService adminBookEditService;

	@Autowired
	private StatusService statusService;

	@Autowired
	private LibraryService libraryService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private GenreService genreService;


	@RequestMapping (value = "/admin/bookEdit/{id}", method = RequestMethod.GET)
	public String bookEdit(Model model, @PathVariable int id){

		List<LibraryDto> library = libraryService.getLibrary();
		List<CategoryDto> category = categoryService.getCategory();
		List<GenreDto> genre = genreService.getGenre();
		List<StatusDto> status = statusService.getStatus();

		model.addAttribute("libraries", library);
		model.addAttribute("categories", category);
		model.addAttribute("genres", genre);
		model.addAttribute("statuses", status);

		BookDto book = adminBookEditService.getBook(id);

		model.addAttribute("bookForm", book);


		return "adminBookEdit";

	}

	@RequestMapping(value = "/admin/bookEdit/{id}", method = RequestMethod.POST)
	public String bookUpdate(Model model, @ModelAttribute BookForm form){

		BookDto dto = new BookDto();
		BeanUtils.copyProperties(form, dto);

		adminBookEditService.updateBook(dto);

		return "redirect:/admin/bookList/";



	}

}
