package jp.co.alhinc.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.dto.BorrowDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.form.BorrowForm;
import jp.co.alhinc.form.ReserveForm;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminBookListService;
import jp.co.alhinc.service.AdminUserListService;
import jp.co.alhinc.service.BorrowService;
import jp.co.alhinc.service.ReserveService;

@Controller
public class BorrowSearchController {



	@Autowired
	private BorrowService borrowService;

	@Autowired
	private AdminBookListService adminBookListService;

	@Autowired
	private AdminUserListService adminUserListService;

	@Autowired
	private ReserveService reserveService;

	@RequestMapping(value = "/admin/checkOut/", method = RequestMethod.GET)
	public String borrowBook(@ModelAttribute("borrowForm")BorrowForm borrowForm,BindingResult result,Model model){

		if (result.hasErrors()) {
		        model.addAttribute("message", "以下のエラーを解消してください");
		 }else if (borrowForm.getBook_id() != 0) {
			 BookForm bookForm = new BookForm();
			 bookForm.setId(borrowForm.getBook_id());
			 List<BookDto> bookList = adminBookListService.getBookAll(bookForm);
			 model.addAttribute("bookList",bookList);
		 }

		if (result.hasErrors()) {
		        model.addAttribute("message", "以下のエラーを解消してください");
		 }else if (borrowForm.getUser_num() != null && borrowForm.getUser_num().length() != 0){
			 UserForm userForm = new UserForm();
			 userForm.setNum(borrowForm.getUser_num());
			 List<UserDto>  userList = adminUserListService.getUserAll(userForm);
			 model.addAttribute("userList",userList);
			 borrowForm.setUser_id(userList.get(0).getId());
		}

		model.addAttribute("borrowForm", borrowForm);
		BorrowForm borrow = new BorrowForm();
		model.addAttribute("checkOutForm", borrow);
		return "borrowSearch";


	}

	@RequestMapping (value = "/admin/checkOut/", method = RequestMethod.POST)
	public String borrowSignup(Model model,@ModelAttribute ("checkOutForm")BorrowForm checkOutForm){



			BorrowDto dto = new BorrowDto();

			BeanUtils.copyProperties(checkOutForm, dto);


			Date date = new Date();

			Calendar calendar = Calendar.getInstance();

			calendar.setTime(date);

			calendar.add(Calendar.DATE,14);
			Date return_schedule = calendar.getTime();

			dto.setReturn_schedule(return_schedule);

			borrowService.borrowRegister(dto);


			BookForm bookForm = new BookForm();

			bookForm.setStatus(2);
			bookForm.setId(checkOutForm.getBook_id());

			borrowService.changeStatus(bookForm);

			ReserveForm reserveForm = new ReserveForm();
			reserveForm.setLending(1);
			reserveForm.setBook_id(checkOutForm.getBook_id());
			reserveForm.setUser_id(checkOutForm.getUser_id());

			System.out.println(reserveForm.getLending());
			reserveService.changeLending(reserveForm);


			List<BorrowDto> borrowSelect = borrowService.getReturnNew(checkOutForm);
			model.addAttribute("borrowSelect",borrowSelect);




			return "borrowList";
		}


}
