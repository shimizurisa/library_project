package jp.co.alhinc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class adminReturnedController {

	    @RequestMapping(value = "/admin/returned/{id}", method = RequestMethod.GET)
	    public String showMessage(Model model, @PathVariable int id) {
	    	model.addAttribute("user_id", id);
	        model.addAttribute("message", "返却が完了しました。");
	        return "adminReturned";
	    }


}
