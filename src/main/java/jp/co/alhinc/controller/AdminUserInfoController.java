package jp.co.alhinc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminUserListService;

@Controller
public class AdminUserInfoController {

	@Autowired
    private AdminUserListService adminUserListService;

    @RequestMapping(value = "/admin/userInfo/{id}", method = RequestMethod.GET)
    public String userInfo(Model model, @PathVariable int id) {

/*    	UserDto info = adminUserInfoService.getUser(id);
    	model.addAttribute("infoForm", info);*/

    	UserForm form = new UserForm();
    	form.setId(id);

        List<UserDto> info = adminUserListService.getUserAll(form);
        model.addAttribute("info", info.get(0));

/*        UserDto user = adminUserInfoService.getUser(id);
        model.addAttribute("userId", user);*/

        return "adminUserInfo";
    }

}
