package jp.co.alhinc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserHomeController {

	@RequestMapping(value = "//", method = RequestMethod.GET)
	public String adminHome(Model model){

		return "userHome";
	}

}
