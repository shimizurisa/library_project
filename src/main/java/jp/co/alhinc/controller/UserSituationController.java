package jp.co.alhinc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BorrowDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.BorrowForm;
import jp.co.alhinc.service.AdminReturnService;

@Controller
public class UserSituationController {

	@Autowired
	private AdminReturnService adminReturnService;

	@Autowired
	HttpSession session;

    @RequestMapping(value = "/userSituation/", method = RequestMethod.GET)
    public String borrowInfo(Model model) {

    	UserDto loginUser = (UserDto) session.getAttribute("loginUser");

    	if(loginUser == null){

    		return "redirect:/login/";
    	}
    	model.addAttribute("user_name", loginUser.getName());

    	BorrowForm borrowForm = new BorrowForm();
    	borrowForm.setUser_id(loginUser.getId());

    	List<BorrowDto> borrowList = adminReturnService.getBorrowList(borrowForm);

    	model.addAttribute("borrowList", borrowList);


    	return "userSituation";

    }

}
