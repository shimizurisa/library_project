package jp.co.alhinc.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.GenderDto;
import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminUserEditService;
import jp.co.alhinc.service.GenderService;
import jp.co.alhinc.service.LibraryService;

@Controller

public class AdminUserEditController {

	@Autowired
	private AdminUserEditService adminUserEditService;

	@Autowired
	private GenderService genderService;

	@Autowired
	private LibraryService libraryService;

	@RequestMapping(value = "/admin/userEdit/{id}", method = RequestMethod.GET)
	public String adminUserEdit(Model model, @PathVariable int id){

		List<GenderDto> gender = genderService.getGender();
		List<LibraryDto> library = libraryService.getLibrary();


		model.addAttribute("gender", gender );
		model.addAttribute("library", library );


		UserDto user = adminUserEditService.getUser(id);

		model.addAttribute("userForm",user);

		return "adminUserEdit";
	}

	@RequestMapping(value = "/admin/userEdit/{id}",method = RequestMethod.POST)
	public String UserUpdate(Model model, @ModelAttribute UserForm form){

		UserDto dto = new UserDto();
		BeanUtils.copyProperties(form, dto);
//		System.out.println(form.getGender() + " " + form.getLibrary_num());
		adminUserEditService.updateUserEdit(dto);

		return "redirect:/admin/userList/";
		}
}
