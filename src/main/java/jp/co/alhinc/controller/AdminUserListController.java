package jp.co.alhinc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.GenderDto;
import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminUserListService;
import jp.co.alhinc.service.GenderService;
import jp.co.alhinc.service.LibraryService;

@Controller
public class AdminUserListController {

	@Autowired
	private AdminUserListService adminUserListService;

	@Autowired
	private GenderService genderService;
	@Autowired
	private LibraryService libraryService;

	@RequestMapping(value = "/admin/userList",method = RequestMethod.GET)
	public String userAll(Model model, @ModelAttribute("searchForm") UserForm form){

		//プルダウン、ラジオボタンの項目の情報を持ってきた
		List<LibraryDto> library = new ArrayList<>();
		LibraryDto libDto = new LibraryDto();
		libDto.setId(0);
		libDto.setName("すべて");
		library.add(libDto);
		List<LibraryDto> library2 = libraryService.getLibrary();
		library.addAll(library2);

		model.addAttribute("libraries", library);


		List<GenderDto> gender = new ArrayList<>();
		GenderDto genderDto = new GenderDto();
		genderDto.setId(0);
		genderDto.setGender("指定なし");
		gender.add(genderDto);
		List<GenderDto> gender2 = genderService.getGender();
		gender.addAll(gender2);

		model.addAttribute("genders", gender);


		model.addAttribute("searchForm", form);

		//ユーザー一覧の情報をDBから取ってきた
		List<UserDto> users = adminUserListService.getUserAll(form);

		model.addAttribute("users", users);

		return "adminUserList";
	}
}
