package jp.co.alhinc.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BookDto;
import jp.co.alhinc.dto.CategoryDto;
import jp.co.alhinc.dto.GenreDto;
import jp.co.alhinc.dto.LibraryDto;
import jp.co.alhinc.dto.StatusDto;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.service.BookSignUpService;
import jp.co.alhinc.service.CategoryService;
import jp.co.alhinc.service.GenreService;
import jp.co.alhinc.service.LibraryService;
import jp.co.alhinc.service.StatusService;

@Controller
public class BookSignUpController{

	@Autowired
	private BookSignUpService bookSignUpService;

	@Autowired
	private StatusService statusService;

	@Autowired
	private LibraryService libraryService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private GenreService genreService;

	@RequestMapping (value = "/admin/bookSignup/", method = RequestMethod.GET)
	public String bookSignUp(@ModelAttribute("bookForm") BookForm form, BindingResult result, Model model){


		List<LibraryDto> library = libraryService.getLibrary();
		List<CategoryDto> category = categoryService.getCategory();
		List<GenreDto> genre = genreService.getGenre();
		List<StatusDto> status = statusService.getStatus();

		model.addAttribute("libraries", library);
		model.addAttribute("categories", category);
		model.addAttribute("genres", genre);
		model.addAttribute("statuses", status);

		if (result.hasErrors()) {
	        model.addAttribute("title", "エラー");
	        model.addAttribute("message", "以下のエラーを解消してください");
	    } else {

		model.addAttribute("bookForm", form);
	    }

		return "bookSignUp";


	}

	@RequestMapping (value = "/admin/bookSignup/", method = RequestMethod.POST)
	public String bookSignUp(@ModelAttribute BookForm form, Model model){

		BookDto dto = new BookDto();

		BeanUtils.copyProperties(form, dto);
		bookSignUpService.bookRegister(dto);

		return "redirect:/admin/bookList/";
	}
}
