package jp.co.alhinc.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.BorrowDto;
import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.BookForm;
import jp.co.alhinc.form.BorrowForm;
import jp.co.alhinc.form.ReserveForm;
import jp.co.alhinc.service.AdminBookListService;
import jp.co.alhinc.service.AdminReturnService;
import jp.co.alhinc.service.ReserveService;

@Controller
public class AdminReturnController {

	  @Autowired
	    private AdminReturnService adminReturnService;
	  @Autowired
	    private AdminBookListService adminBookListService;
	  @Autowired
	    private ReserveService reserveService;
	  @Autowired
	    HttpSession session;

	  @RequestMapping(value = "/admin/return/{id}", method = RequestMethod.GET)
	  public String userBorrowing(Model model, @PathVariable int id){

		  UserDto dto = (UserDto) session.getAttribute("returnUser");
		  model.addAttribute("user_name", dto.getName());


		  BorrowForm form = new BorrowForm();
		  form.setUser_id(id);


		  List<BorrowDto> borrow = adminReturnService.getBorrowList(form);

		  model.addAttribute("borrow", borrow);
//		  model.addAttribute("user_name", borrow.get(0));

//		  List<BorrowDto> reserved = adminReturnService.getReservedList(form);
//		  model.addAttribute("reserved", reserved);

		  return "adminReturn";

	  }



		@RequestMapping(value = "/admin/return/{id}", method = RequestMethod.POST)
		public String returnUpdate(Model model, @ModelAttribute ("returnForm") BorrowForm returnForm, @PathVariable int id){

			BookForm bookForm = new BookForm();
			bookForm.setId(returnForm.getBook_id());
			bookForm.setStatus(3);
			adminBookListService.changeStatus(bookForm);

			ReserveForm reserveForm = new ReserveForm();
			reserveForm.setBook_id(returnForm.getBook_id());
			reserveForm.setUser_id(returnForm.getUser_id());
			reserveForm.setLending(9);
			reserveService.changeLending(reserveForm);

			BorrowForm borrowForm = new BorrowForm();
			borrowForm.setId(returnForm.getId());
			borrowForm.setReturn_date(returnForm.getReturn_date());
			borrowForm.setUpdate_date(returnForm.getUpdate_date());
			adminReturnService.updateStatus(borrowForm);


			return "redirect:../../admin/returned/"+ id;



		}



}
