package jp.co.alhinc.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.alhinc.dto.UserDto;
import jp.co.alhinc.form.UserForm;
import jp.co.alhinc.service.AdminReturnService;
import jp.co.alhinc.service.AdminUserListService;

@Controller

public class AdminInputNumController {

	@Autowired
	AdminReturnService adminReturnService;

	@Autowired
	HttpSession messageSession;
	@Autowired
	HttpSession session;
	@Autowired
	AdminUserListService adminUserListService;


	@RequestMapping(value="/admin/inputNum/", method=RequestMethod.GET)
	public String inputNum(Model model){

		UserForm userForm = new UserForm();
		model.addAttribute("userForm", userForm);

		return "adminInputNum";

	}

	@RequestMapping(value="/admin/inputNum/", method=RequestMethod.POST)
	public String inputNum(Model model, @ModelAttribute("userForm")UserForm userForm){

		List<UserDto> userList = adminUserListService.getUserAll(userForm);
		List<String> messages = new ArrayList<>();

		if(userList.size()==0){
			messages.add("利用者証番号をもう一度入力してください");
			messageSession.setAttribute("messages", messages);

			return "adminInputNum";

		}else if (2 <= userList.size()){
			model.addAttribute("userDto", new UserDto());

			throw new IllegalStateException("2 <= userList.size()");


		}else {
			session.setAttribute("returnUser", userList.get(0));

		}


		int user_id = userList.get(0).getId();

		return "redirect:/admin/return/"+user_id;


	}
}
