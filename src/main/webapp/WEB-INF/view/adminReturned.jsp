<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>返却完了画面</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>
	<h1>返却完了</h1>

		<a href = "../../admin/" id="square_btn">ホーム</a>
		<a href = "../../admin/bookList/" id="square_btn">図書一覧</a>
		<a href = "../../admin/return/${id}" id="square_btn">返却へ戻る</a>



	</body>
</html>