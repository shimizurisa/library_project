<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>図書編集</title>
	</head>
	<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	<body>

		<h1>図書編集</h1>

		<a href = "../bookList/">図書一覧</a>
		<a href = "../">ホーム</a>

		<form:form modelAttribute = "bookForm">

			<form:input path="id" type="hidden"/>
			<p>図書館名<form:select path="lib_num" items="${libraries}" itemLabel="name" itemValue="id"/></p>
			<p>棚番号<form:input path="shelf_num" /></p>
			<p>ISBN<form:input path="isbn"/></p>
			<p>書名<form:input path="book_name"/></p>
			<p>著者名<form:input path="author"/></p>
			<p>出版社<form:input path="publisher"/></p>
			<p>カテゴリー<form:select path="category" items="${categories}" itemLabel="name" itemValue="id"/></p>
			<p>ジャンル<form:select path="genre" items="${genres}" itemLabel="name" itemValue="id"/></p>
			<p>状態<form:radiobuttons path="status" items="${statuses}" itemLabel="name" itemValue="id"/></p>



			<input type = "submit" value="更新" id="square_btn">

		</form:form>


</body>
</html>