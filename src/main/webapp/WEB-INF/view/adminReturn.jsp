<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>返却画面</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>


		<h1>${user_name}さん</h1>


		<a href = "../../admin/" id="square_btn" >ホーム</a>
		<a href = "../../admin/bookList/" id="square_btn" >図書一覧</a><!--
		<a href = "../admin/reserverMail/">予約連絡</a> -->
		<a href = "../../admin/inputNum/"id="square_btn" >利用者証番号入力</a>


		<h2>借りている本</h2>
		<table border="1">
			<tr>
				<th>書名</th>
				<th>著者名</th>
				<th>貸出日</th>
				<th>返却予定日</th>
				<th>返却</th>
			</tr>


			<c:forEach items = "${borrow}" var = "borrow">
<%-- 				<c:if test="${borrow.return_date != null}" > --%>
				<tr>
						<td><c:out value="${borrow.book_name}"></c:out></td>
						<td><c:out value="${borrow.author}"></c:out></td>
						<td><c:out value="${borrow.borrow_date}"></c:out></td>
						<td><c:out value="${borrow.return_schedule}"></c:out></td>
						<td>
						<form:form modelAttribute = "returnForm" method="post">
									<input type="hidden" name="book_id" value="${borrow.book_id}" />
									<input type="hidden" name="user_id" value="${borrow.user_id}" />
									<input type="hidden" name="id" value="${borrow.id}" />
									<input type="submit" name="return" value="返却する" id="square_btn" />
						</form:form>
								<%-- <a href="${pageContext.request.contextPath}/admin/userEdit/${info.id}">返却</a>
						</td> --%>
				</tr>
<%-- 				</c:if> --%>
			</c:forEach>
		</table>







	</body>
</html>