<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>管理者ホーム</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>


	<h1>ホーム</h1>

		<a href = "../admin/inputNum/" id="square_btn">返却</a>
		<a href = "../admin/checkOut/" id="square_btn">貸出</a>

		<a href = "../admin/bookList/" id="square_btn">図書一覧</a>
		<a href = "../admin/bookSignup/" id="square_btn">図書登録</a>

		<a href = "../admin/userList/" id="square_btn">ユーザー一覧</a>
		<a href = "../admin/userSignup/" id="square_btn">ユーザー登録</a>

	</body>
</html>