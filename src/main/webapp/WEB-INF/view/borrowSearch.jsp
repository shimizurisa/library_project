<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>貸出画面</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>

	<h1>貸出画面</h1>

	<a href = "../">ホーム</a>

	<form:form modelAttribute="borrowForm" method="get">
	<div><form:errors path="id"  /></div>

        <p>図書一連番号<form:input path="book_id" /></p>
        <p>利用者証番号<form:input path="user_num" /></p>

        <input type="submit" id="square_btn">
	</form:form>

	<c:forEach items="${userList}" var="user">

		<p>利用者証番号:<c:out value="${user.num}"/></p>
		<p>名前:<c:out value="${user.name}"/></p>


		<div>以下の書籍の貸出し処理を行います</div><br/>


		<c:forEach items="${bookList}" var="book">

			<table border="1">
				<tr>
					<th>図書一連番号</th>
					<th>書名</th>
					<th>著者名</th>
				</tr>

				<tr>
					<td><c:out value="${book.id}"/></td>
					<td><c:out value="${book.book_name}"/></td>
					<td><c:out value="${book.author}"/></td>
				</tr>


			</table>
			<form:form modelAttribute = "checkOutForm" method = "post">
				<%-- <input type="hidden" name="id" value="${user.id}"/> --%>
				<input type="hidden" name="book_id" value="${book.id}"/>
				<input type="hidden" name="user_id" value="${user.id}"/>
				<input type="submit" value = "貸出" id="square_btn"/>
			</form:form>


		</c:forEach>
	</c:forEach>

	</body>
</html>