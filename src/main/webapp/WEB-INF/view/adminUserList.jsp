<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー一覧</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>

		<h1>ユーザー一覧</h1>
		<a href = "../" id="square_btn">ホーム</a>
		<a href = "../userSignup/" id="square_btn">ユーザー登録</a>



		<form:form modelAttribute = "searchForm" method="get">

			<form:hidden path="id" />

			<p>利用者証番号<form:input path="num"/></p>
			<p>名前<form:input path="name"/></p>
			<p>性別<form:radiobuttons path="gender" items="${genders}" itemLabel="gender" itemValue="id"/></p>
			<p>住所<form:input path="address"/></p>
			<p>電話<form:input path="tel"/></p>
			<p>メール<form:input path="mail"/></p>
			<p>受取図書館<form:select path="library_num" items="${libraries}" itemLabel="name" itemValue="id"/></p>


			<input type = "submit" value="検索" id="square_btn"/>

		</form:form>

		<c:if test="${empty users}">
				<c:out value = "検索条件に該当するユーザーが見つかりませんでした。"/>
		</c:if>
		<c:if test="${not empty users}">

			<table border="1">
				<tr>
					<th>利用者証番号</th>
					<th>名前</th>
					<th>性別</th>
					<th>住所</th>
					<th>電話</th>
					<th>メール</th>
					<th>受取図書館名</th>
					<th>編集</th>
				</tr>

				<c:forEach items = "${users}" var = "user">

					<tr>
						<td><c:out value="${user.num}"></c:out></td>
						<td><c:out value="${user.name}"></c:out></td>
						<td><c:out value="${user.gender_name}"></c:out></td>
						<td><c:out value="${user.address}"></c:out></td>
						<td><c:out value="${user.tel}"></c:out></td>
						<td><c:out value="${user.mail}"></c:out></td>
						<td><c:out value="${user.library_name}"></c:out></td>
						<%-- <td><c:out value="${book.updated_date}"></c:out></td> --%>
						<td><a href="${pageContext.request.contextPath}/admin/userEdit/${user.id}" id="square_btn">編集</a></td>
					</tr>
				</c:forEach>
			</table>
		</c:if>
	</body>
</html>