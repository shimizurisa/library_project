<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
</head>
<body>

	<h1>ユーザー編集</h1>

	<a href = "../userList/" id="square_btn">ユーザー一覧</a>
		<a href = "../" id="square_btn">ホーム</a>

		<form:form modelAttribute = "userForm">

			<form:input path="id" type="hidden"/>
			<p>利用者証番号<form:input path="num"/></p>
			<p>名前<form:input path="name"/></p>
			<p>性別<form:select path="gender" items="${gender}" itemLabel="gender" itemValue="id"/></p>
			<p>住所<form:input path="address"/></p>
			<p>電話<form:input path="tel"/></p>
			<p>メール<form:input path="mail"/></p>
			<p>受取図書館<form:select path="library_num" items="${library}" itemLabel="name" itemValue="id"/></p>

		<input type = "submit" value="更新" id="square_btn">
		</form:form>

</body>
</html>