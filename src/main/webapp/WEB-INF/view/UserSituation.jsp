<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>利用状況照会</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
		<body>

			<h1>利用状況照会</h1>

			<a href = "../" id="square_btn">ホーム</a>
			<a href = "../search/" id="square_btn">図書一覧</a>
			<a href="../logout/" id="square_btn">ログアウト</a>

			<h3>${user_name}さん</h3>

			<h2>借りている本</h2>

			<c:if test="${empty borrowList}">
				<c:out value = "現在、借りている本はありません。"/>

			</c:if>
			<c:if test="${not empty borrowList}">

				<table border="1">
					<tr>
						<th>書名</th>
						<th>著者名</th>
						<th>貸出日</th>
						<th>返却期限</th>

					</tr>


					<c:forEach items = "${borrowList}" var = "borrow">

						<tr>
								<td><c:out value="${borrow.book_name}"></c:out></td>
								<td><c:out value="${borrow.author}"></c:out></td>
								<td><c:out value="${borrow.borrow_date}"></c:out></td>
								<td><c:out value="${borrow.return_schedule}"></c:out></td>

						</tr>

					</c:forEach>
				</table>
			</c:if>

			<h2>予約している本</h2>

			<c:if test="${empty reserveList}">
				<c:out value = "現在、予約している本はありません"/>

			</c:if>
			<c:if test="${not empty reserveList}">
				<table border="1">
					<tr>
						<th>書名</th>
						<th>著者名</th>
						<th>予約日</th>
						<th>受取図書館到着日</th>

					</tr>


					<c:forEach items = "${reserveList}" var = "reserve">
						<tr>
							<td><c:out value="${reserve.book_name}"></c:out></td>
							<td><c:out value="${reserve.author}"></c:out></td>
							<td><c:out value="${reserve.reserved_date}"></c:out></td>
							<td><c:out value="${reserve.arrive_date}"></c:out></td>
						</tr>
					</c:forEach>
				</table>
			</c:if>


		</body>
</html>