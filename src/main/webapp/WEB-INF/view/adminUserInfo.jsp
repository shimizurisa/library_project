<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>各ユーザー情報</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">

	</head>
	<body>

		<a href = "../admin/">ホーム</a>
		<a href = "../admin/userList/">ユーザー一覧</a>

		<h1>${info.name}さんの情報</h1>

		<table border="1">
			<tr>
				<th>利用者証番号</th>
				<th>名前</th>
				<th>性別</th>
				<th>住所</th>
				<th>電話番号</th>
				<th>メールアドレス</th>
				<th>受取図書館</th>
				<th>ユーザー編集</th>
			</tr>



			<tr>
					<td><c:out value="${info.num}"></c:out></td>
					<td><c:out value="${info.name}"></c:out></td>
					<td><c:out value="${info.gender_name}"></c:out></td>
					<td><c:out value="${info.address}"></c:out></td>
					<td><c:out value="${info.tel}"></c:out></td>
					<td><c:out value="${info.mail}"></c:out></td>
					<td><c:out value="${info.library_name}"></c:out></td>
					<td>
							<a href="${pageContext.request.contextPath}/admin/userEdit/${info.id}" id="square_btn">編集</a>
					</td>
			</tr>
		</table>


</body>
</html>