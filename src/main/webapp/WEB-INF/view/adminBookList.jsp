<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>図書一覧(管理)</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>

	<body>

		<h1>図書一覧</h1>
		<a href = "../bookSignup/" id="square_btn">図書登録</a>
		<a href = "../" id="square_btn">ホーム</a>


		<form:form modelAttribute = "searchForm" method="get">

<%-- 			<form:hidden path="id" /> --%>
			<p>図書館名<form:select path="lib_num" items="${libraries}" itemLabel="name" itemValue="id"/></p>
<%-- 			<p>ISBN<form:input path="isbn"/></p> --%>
			<p>書名<form:input path="book_name"/></p>
			<p>著者名<form:input path="author"/></p>
			<p>出版社<form:input path="publisher"/></p>
			<p>カテゴリー<form:select path="category" items="${categories}" itemLabel="name" itemValue="id"/></p>
			<p>ジャンル<form:select path="genre" items="${genres}" itemLabel="name" itemValue="id"/></p>
			<p>状態<form:radiobuttons path="status" items="${statuses}" itemLabel="name" itemValue="id"/></p>

			<input type = "submit" value="検索" id="square_btn"/>

		</form:form>

		<c:if test="${empty books}">
				<c:out value = "検索条件に該当する図書が見つかりませんでした。"/>
		</c:if>
		<c:if test="${not empty books}">

			<table border="1">
				<tr>
					<th>図書館名</th>
					<th>図書一連番号</th>
					<th>棚番号</th>
					<th>ISBN</th>
					<th>書名</th>
					<th>著者名</th>
					<th>出版社</th>
					<th>カテゴリー</th>
					<th>ジャンル</th>
					<th>状態</th>
					<th>状態の切り替え</th>
					<th>編集</th>
				</tr>

				<c:forEach items = "${books}" var = "book">

					<tr>
						<td><c:out value="${book.lib_name}"></c:out></td>
						<td><c:out value="${book.id}"></c:out></td>
						<td><c:out value="${book.shelf_num}"></c:out></td>
						<td><c:out value="${book.isbn}"></c:out></td>
						<td><c:out value="${book.book_name}"></c:out></td>
						<td><c:out value="${book.author}"></c:out></td>
						<td><c:out value="${book.publisher}"></c:out></td>
						<td><c:out value="${book.category_name}"></c:out></td>
						<td><c:out value="${book.genre_name}"></c:out></td>
						<td><c:out value="${book.status_name}"></c:out></td>
						<td>
						<c:if test = "${book.status == 3}">
							<form:form modelAttribute = "bookForm" method = "post">
								<input type="hidden" name="status" value="1"/>
								<%-- <form:hidden path="id"/> --%>
								<input type="hidden" name="id" value="${book.id}"/>
								<input type = "submit" value = "棚保管中へ" />
							</form:form>
						</c:if>

						<td>
							<a href="${pageContext.request.contextPath}/admin/bookEdit/${book.id}" id="square_btn">編集</a>
						</td>
					</tr>

					</c:forEach>
			</table>
		</c:if>
	</body>
</html>