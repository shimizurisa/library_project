<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>予約</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>

		<h1>予約</h1>

		<a href="../logout/" id="square_btn">ログアウト</a>
		<a href="../" id="square_btn">ホーム</a>
	<br/>

		<table border="1">
			<tr>
				<th>図書館名</th>
				<th>棚番号</th>
				<th>ISBN</th>
				<th>書名</th>
				<th>著者名</th>
				<th>出版社</th>
				<th>カテゴリー</th>
				<th>ジャンル</th>
				<th>状態</th>
			</tr>

<%-- 			<c:forEach items = "${reserveBook}" var = "book"> --%>

			<tr>
				<td><c:out value="${reserveBook.lib_name}"></c:out></td>
				<td><c:out value="${reserveBook.shelf_num}"></c:out></td>
				<td><c:out value="${reserveBook.isbn}"></c:out></td>
				<td><c:out value="${reserveBook.book_name}"></c:out></td>
				<td><c:out value="${reserveBook.author}"></c:out></td>
				<td><c:out value="${reserveBook.publisher}"></c:out></td>
				<td><c:out value="${reserveBook.category_name}"></c:out></td>
				<td><c:out value="${reserveBook.genre_name}"></c:out></td>
				<td><c:out value="${reserveBook.status_name}"></c:out></td>
			</tr>

<%-- 				</c:forEach>
--%>	</table>

		<table border="1">
			<tr>
				<th>利用者証番号</th>
				<th>名前</th>
				<th>性別</th>
				<th>住所</th>
				<th>電話番号</th>
				<th>メールアドレス</th>
				<th>受取図書館</th>
			</tr>

			<tr>

				<td><c:out value="${reserveUser.num}"></c:out></td>
				<td><c:out value="${reserveUser.name}"></c:out></td>
				<td><c:out value="${reserveUser.gender_name}"></c:out></td>
				<td><c:out value="${reserveUser.address}"></c:out></td>
				<td><c:out value="${reserveUser.tel}"></c:out></td>
				<td><c:out value="${reserveUser.mail}"></c:out></td>
				<td><c:out value="${reserveUser.library_name}"></c:out></td>


			</tr>
		</table>

		<form:form modelAttribute = "reserveForm">
			<input type="hidden" value="${reserveBook.id}" name="book_id"/>
			<input type="hidden" value="${reserveUser.id}" name="user_id"/>
			<input type = "submit" value="予約を確定する" id="square_btn">
		</form:form>



	</body>
</html>