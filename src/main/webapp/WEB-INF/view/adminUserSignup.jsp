<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー登録</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>

		<h1>ユーザー登録</h1>

		<a href="../" id="square_btn">ホーム</a>
		<a href="../userList/" id="square_btn">ユーザー一覧</a>


		<form:form modelAttribute = "userForm">

			<p>利用者証番号<form:input path="num" /></p>
			<p>名前<form:input path="name" /></p>
			<p>性別<form:select path="gender" items="${genders}" itemLabel="gender" itemValue="id"/></p>
			<p>住所<form:input path="address" /></p>
			<p>電話番号<form:input path="tel" /></p>
			<p>メールアドレス<form:input path="mail" /></p>
			<p>受取図書<form:select path="library_num" items="${libraries}" itemLabel="name" itemValue="id"/></p>

			<input type = "submit" value = "登録" id="square_btn">

		</form:form>

	</body>
</html>