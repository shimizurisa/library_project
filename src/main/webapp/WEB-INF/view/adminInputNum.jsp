<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>利用者証番号入力画面（返却処理）</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
	<body>

		<h1>利用者証番号入力</h1>

		<a href = "../../admin/" id="square_btn">ホーム</a>
		<a href = "../../admin/bookList/" id="square_btn">図書一覧</a>

		<c:if test="${ not empty messages }">
			<div class="messages">
				<ul>
					<c:forEach items="${messages}" var="message">
 						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
 			<c:remove var="messages" scope="session"/>
		</c:if>


		<form:form modeAttribute = "userForm">

			<p>利用者証番号<input type="text" name="num" id="num" /></p>

			<input type = "submit" value ="返却確認へ" id="square_btn"/>

		</form:form>

	</body>
</html>