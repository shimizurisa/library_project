<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>貸出完了画面</title>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
	</head>
		<body>

		<h1>貸出完了画面</h1>

		<a href = "../" id="square_btn">ホーム</a>
		<a href = "../bookList/" id="square_btn">図書一覧</a>

		<br/>
		<br/>

		<c:forEach items = "${borrowSelect}" var = "userborrow">
		<c:out value="${userborrow.user_name}"></c:out>さんの貸出処理<br/><br/>
		</c:forEach>

		<div>貸出を完了しました</div><br/>

		<table border="1">
			<tr>
				<th>図書一連番号</th>
				<th>書名</th>
				<th>著者名</th>
				<th>返却予定日</th>
			</tr>

			<c:forEach items = "${borrowSelect}" var = "borrow">
				<tr>
					<td><c:out value="${borrow.book_id}"></c:out></td>
					<td><c:out value="${borrow.book_name}"></c:out></td>
					<td><c:out value="${borrow.author}"></c:out></td>
					<td><fmt:formatDate value="${borrow.return_schedule}" pattern="yyyy年MM月dd日" /></td>
				</tr>
			</c:forEach>
		</table>

		</body>
</html>